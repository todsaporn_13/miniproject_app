import 'package:flutter/material.dart';         //หน้า1.1.1
 
void main() {
  runApp(MyApp());
}
 
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}
 
class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            backgroundColor:Color(0xff00BCD1) ,
            title: Text('ยืมคืนอุปกรณ์'),
          ),
/*-------------------------------------- */
          body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/lake.jpg"),
            fit: BoxFit.cover,
          ),
        ),

        /*-------------------------------------- */
          child: ListView(children: <Widget>[
            Center(
                child: Text(
              'equipment',
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            )),
            DataTable(
              columns: [
                DataColumn(label: Text('id')),
                DataColumn(label: Text('Name')),
                DataColumn(label: Text('Unit')),
              ],
              rows: [
                DataRow(cells: [
                  DataCell(Text('76')),
                  DataCell(Text('monitor')),
                  DataCell(Text('10')),
                ]),
                DataRow(cells: [
                  DataCell(Text('78')),
                  DataCell(Text('keyboard')),
                  DataCell(Text('12')),
                ]),
                DataRow(cells: [
                  DataCell(Text('79')),
                  DataCell(Text('notebook')),
                  DataCell(Text('11')),
                ]),
              ],
            ),
          ])),
    ));
  }
}

