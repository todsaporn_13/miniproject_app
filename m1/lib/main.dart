import 'package:flutter/material.dart';    //หน้า 1
void main() {  
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(20),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'Account',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  'ID : 601',
                  style: TextStyle(
                    color: Colors.blue[700],
                  ),
                ),
                Text(
                  'Password : ******',
                  style: TextStyle(
                    color: Colors.blue[700],
                  ),
                ),
                Text(
                  'user name : Toddy',
                  style: TextStyle(
                    color: Colors.blue[700],
                  ),
                ),
                Text(
                  'Email : Todplay@gmail.com',
                  style: TextStyle(
                    color: Colors.blue[700],
                  ),
                ),
              ],
            ),
          ),

            
        ],
      ),
    );


    Widget textSection = Container();
    return MaterialApp(
     // title: 'Flutter layout demo',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff00BCD1),
          title: Text('ยืมคืนอุปกรณ์'),
        ),

        body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/lake.jpg"),
            fit: BoxFit.cover,
          ),
        ),

        child: ListView(
          children: [
            Image.asset(
              'images/user.jpg',
                scale: 1,
            ),
            titleSection,
            textSection,

            Container(
                  height: 50,
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Color(0xff00BCD1),
                      child: Text('ทำรายการตรวจเช็คอุปกรณ์'),
                      onPressed: () {},
                    )),
             Container(
                  height: 50,
                    padding: EdgeInsets.fromLTRB(80, 10, 80, 0),
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Color(0xff00BCD1),
                      child: Text('กลับไปหน้าเข้าสู่ระบบ'),
                      onPressed: () {},
                    )),
          ],
        ),
      ),
    ));
  }
}
 
 
    

