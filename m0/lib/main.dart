import 'package:flutter/material.dart';         //หน้า0

void main() {
  runApp(MaterialApp(
    home: MyApp(),
  ));
}
class MyApp extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<MyApp> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF004D40),
          title: Text('ยืมคืนอุปกรณ์'),
          
        ),
        /*------------------------------------- */
        body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/lake.jpg"),
            fit: BoxFit.cover,
          ),
        ),

        /*------------------------------------- */
        child: ListView(
              children: <Widget>[
               
                 Container(
                  margin: EdgeInsets.all(15),
                      child: Icon(
                        //Icons.lightbulb_outline,
                        Icons.build,    
                        //Icons.new_releases, 

                        color: Colors.orange[600],
                        //color: Colors.blue[600], 
                        //color: Colors.brown[800],                
                        size: 150,
                  ),
                ),

                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(10),
                    child: Text(
                      'เข้าสู่ระบบ',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 30),
                    )),
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    controller: nameController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'ชื่อผู้ใช้',
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 20),
                  child: TextField(
                    obscureText: true,
                    controller: passwordController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'รหัสผู้ใช้',
                    ),
                  ),
                ),
                
                Container(
                  height: 50,
                    padding: EdgeInsets.fromLTRB(50, 0, 50, 10),
                    child: RaisedButton(
                      textColor: Colors.white,
                       color: Colors.blueGrey[600],
                      child: Text('เข้าสู่ระบบ'),
                      onPressed: () {
                        print(nameController.text);
                        print(passwordController.text);
                      },
                    )),

                    Container(
                  height: 50,
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Colors.red[800],
                      child: Text('สมัครสมาชิก'),
                      onPressed: () {
                        print(nameController.text);
                        print(passwordController.text);
                      },
                    )),
                


              ],
            )
               
                  )
                );

  }
            
            
  }




/*----------------------- */
